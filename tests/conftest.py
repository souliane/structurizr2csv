import pytest
from structurizr2csv.convert import Converter


@pytest.fixture(scope="class")
def converter():
    return Converter()
