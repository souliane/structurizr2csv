import difflib
import filecmp
import re
import sys
from pathlib import Path
from typing import List
from unittest.mock import patch

import pytest


class TestConvert:

    TIMESTAMP_PATTERN = r'"[A-Z][a-z]+ \d{1,2}\. [A-Z][a-z]+ \d{4}, \d{2}:\d{2}:\d{2}"'

    @classmethod
    def remove_timestamps(cls, filepath: Path) -> List[str]:
        """Remove timestamps from CSV files, otherwise the tests will always fail."""
        with open(filepath, "r") as fp:
            return [re.sub(cls.TIMESTAMP_PATTERN, "", line) for line in fp.readlines()]

    @pytest.mark.parametrize(
        "workspace_json",
        ["big-bank-plc.dsl", "financial-risk-system.dsl", "groups.dsl", "shapes.dsl"],
    )
    def test_convert(self, workspace_json: str, converter):

        with patch.object(
            sys, "argv", ["structurizr2csv", f"examples/{workspace_json}"]
        ):
            converter.run()

        stem = Path(workspace_json).stem
        result = filecmp.dircmp(f"output/{stem}", f"tests/assets/output/{stem}")

        assert not any((result.left_only, result.right_only))

        for filename in result.diff_files:
            file_result = list(
                difflib.unified_diff(
                    self.remove_timestamps(Path(result.left) / filename),
                    self.remove_timestamps(Path(result.right) / filename),
                )
            )
            try:
                assert not file_result
            except AssertionError as e:
                sys.stdout.writelines(file_result)
                raise AssertionError from e
