FROM python:3.10

WORKDIR /structurizr2csv

COPY . .

RUN pip install -r requirements.txt
RUN pip install -r requirements-dev.txt
RUN pip install -e .

RUN apt update
RUN apt install -y docker.io  # to convert workspace DSL to JSON with structurizr/cli

CMD [ "/bin/bash" ]