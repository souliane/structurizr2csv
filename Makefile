.DEFAULT_GOAL := help
.PHONY: tests

DOCKER_IMAGE = souliane/structurizr2csv:latest
DOCKER_IMAGE_CLI = souliane/structurizr-cli-with-graphviz:latest

# The path /structurizr2csv must match structurizr2csv.settings.DOCKER_MOUNT_TARGET
# We expose docker socket to fake "docker in docker" (dind)
RUN = docker run -it -v //var/run/docker.sock:/var/run/docker.sock -v `pwd`:/structurizr2csv $(DOCKER_IMAGE)

TEST_ARGS = tests --maxfail=1 --failed-first -s
PIP = python3 -m pip install

docker_build_cli:  ## Build Docker image for structurizr/cli with graphviz
	docker image build -f Dockerfile.structurizr_cli_with_graphviz -t $(DOCKER_IMAGE_CLI) .

docker_build:  ## Build Docker image
	docker image build . -t $(DOCKER_IMAGE)

docker_bash:  # Run Docker image 
	$(RUN) $(ARGS)

requirements:  ## Install requirements
	$(PIP) -r requirements.txt -r requirements-dev.txt $(ARGS)

formatting:  ## Auto-formatting with black and isort
	python3 -m black src; python3 -m isort src

tox:  ## Run tox
	PYTHONPATH=src python3 -m tox

tests:  ## Run tests
	PYTHONPATH=src python3 -m pytest $(TEST_ARGS) $(ARGS)


## The following are used by the GitLab CI:

black_check:
	$(PIP) -r requirements-dev.txt && python3 -m black --check src

isort_check:
	$(PIP) -r requirements-dev.txt && python3 -m isort --check src

pytest_verbose:
	$(PIP) -r requirements.txt && python3 -m pytest -v
