Those examples of workspaces come from structurizr's DSL repository, you can find them
[here](https://github.com/structurizr/dsl/tree/master/examples).


Copyright owner: Simon Brown (structurirz)

Licence: [Apache License 2.0](https://github.com/structurizr/dsl/blob/master/LICENSE)
